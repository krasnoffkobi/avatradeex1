app.factory('fetch', ['$window', function(url) {
    
}]);

// routing between pages
app.config(function ($routeProvider) {
    $routeProvider
    .when("/stage1", {
        templateUrl: '/tmpl/stage1.html', controller: 'stage1Ctrl'
    })
    .when("/stage2", {
        templateUrl: '/tmpl/stage2.html', controller: 'stage2Ctrl'
    })
    .when("/stage3", {
        templateUrl: '/tmpl/stage3.html', controller: 'stage3Ctrl'
    })
    .when("/stage4", {
        templateUrl: '/tmpl/stage4.html', controller: 'stage4Ctrl'
    })
    .otherwise({ templateUrl: '/tmpl/stage1.html', controller: 'stage1Ctrl' });
});

app.controller('MainController', function($uibModal, $scope, $http, $rootScope, $location) {
    $rootScope.genereData = [];
    $rootScope.mySelect = {
        genre: -1,
        subGenre: -1,
        stage3Form: {
            Required: false,
            SubGenreName: ''
        },
        stage4Form: {
            Booktitle: '',
            Author: '',
            ISBN: '',
            Publisher: 0,
            DatePublished: '',
            NumOfPages: '',
            Format: 0,
            Edition: '',
            EditionLanguage: 0,
            Description: ''
        },
    };
    $scope.nextDisable = true;
    $scope.path = $location.$$path;

    $scope.$on("EmitGenre", function(evt,data){ 
        $rootScope.mySelect.genre = data;
        $scope.nextDisable = $scope.buttonEnable();
    })

    $scope.$on("EmitSubGenre", function(evt,data){ 
        $rootScope.mySelect.subGenre = data;
        $scope.nextDisable = $scope.buttonEnable();
    })

    $scope.$on("EmitStage3", function(evt,data){ 
        $rootScope.mySelect.stage3Form = data;
        $scope.nextDisable = $scope.buttonEnable();
    })

    $scope.$on("EmitStage4", function(evt,data){ 
        $rootScope.mySelect.stage4Form = data;
        $scope.nextDisable = $scope.buttonEnable();
    })

    $scope.$on('$routeChangeStart', function($event, next, current) { 
        $scope.path = next.$$route ? next.$$route.templateUrl.replace(/\/tmpl/, '').replace(/.html/, '') : '';
        modalInstance.close();
    });
    
    $http.get('/mockdata/data.json')
        .then(function(response) {
            $rootScope.genereData = response.data;
        });

    $scope.postData = function(url, data) {
        $http.post(url, data)
            .then(function(response) {
                console.log(response);
        });
    }

    $scope.forward = function() {
        switch($location.$$path) {
            case '':
            case '/stage1':
                $location.path('/stage2');
                break;
            case '/stage2':
                if ($rootScope.mySelect.subGenre === 0)
                    $location.path('/stage3');
                else 
                    $location.path('/stage4');
                break;
            case '/stage3':
                $scope.postData('/mockdata/request.json', $rootScope.mySelect.stage3Form)
                $location.path('/stage4');
                break;
            case '/stage4':
                $scope.postData('/mockdata/request.json', $rootScope.mySelect.stage4Form)
                $scope.open();
                break;
            default:
                $location.path('/stage1');
                break;
        }
        $scope.nextDisable = $scope.buttonEnable();
    }

    $scope.back = function () {
        window.history.back();
    };

    $scope.buttonEnable = function() {
        if (($location.$$path === '/stage1' || $location.$$path === '') && $rootScope.mySelect.genre > -1) {
            return false;
        }
        if (($location.$$path === '/stage2') && $rootScope.mySelect.subGenre > -1) {
            return false;
        }
        if (($location.$$path === '/stage3') && $rootScope.mySelect.stage3Form.SubGenreName !== '') {
            return false;
        }
        if ($location.$$path === '/stage4') {
            return false;
        }
        return true;
    }

    let modalInstance;

    $scope.open = function (size, parentSelector) {
        modalInstance = $uibModal.open({
            animation: false,
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl'
          });
      
          modalInstance.result.then(function(selectedItem) {
            console.log('success');
            $location.path('/stage1');
          }, function() {
            console.log('failure')
          });
    };
});

app.controller('ModalInstanceCtrl', function($scope, $uibModalInstance) {

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.ok = function() {
        $uibModalInstance.close('ok');
    }
  });