app.controller('stage3Ctrl', function ($scope, $rootScope) {
    $scope.myForm = {
        SubGenreName: '',
        Required: false
    }

    $scope.onKeyup = function() {
        if ($scope.myForm.SubGenreName !== '') {
            $scope.$emit("EmitStage3",$scope.myForm); 
        }
    }
});