app.controller('stage4Ctrl', function ($scope, $rootScope) {
    $scope.myForm = {
        Booktitle: '',
        Author: '',
        ISBN: '',
        Publisher: 0,
        DatePublished: '',
        NumOfPages: '',
        Format: 0,
        Edition: '',
        EditionLanguage: 0,
        Description: ''
    }

    $scope.$emit("EmitStage4",$scope.myForm); 
});