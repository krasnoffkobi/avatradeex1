app.controller('stage1Ctrl', function ($scope, $rootScope) {
    $scope.genereData = $rootScope.genereData;
    $scope.selectedItem = -1;

    $scope.chooseGenre = function(item) {
        $scope.$emit("EmitGenre",item.id); 
        $scope.selectedItem = item.id;
    }
});