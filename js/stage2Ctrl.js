app.controller('stage2Ctrl', function ($scope, $rootScope) {
    $scope.mySelect = $rootScope.mySelect;
    $scope.genereData = $rootScope.genereData;
    $scope.subGenereData = JSON.parse(JSON.stringify($rootScope.genereData.genres.find(el => el.id === $rootScope.mySelect.genre).subgenres));
    $scope.subGenereData.push({
        id: 0,
        name: "Add New",
        isDescriptionRequired: true
    });

    $scope.chooseSubGenre = function(item) {
        $scope.$emit("EmitSubGenre",item.id); 
        $scope.selectedItem = item.id;
    }
});